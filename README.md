# Metadata Publisher
This fetchs all published studies from our Colectica metadata system and converts necessary information to the da|ra metadata scheme version 4 to make data compliant with the Gesis Vitrine API git indexing method: https://api.vitrine.gesis.org/#GIT-Indexing

## Run script
See gitlab-ci.yml to get an idea of how to get the script run
