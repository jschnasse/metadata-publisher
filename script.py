# API DOC: https://datasearch.fdz.dezim-institut.de/swagger/index.html

import requests, json, xmltodict, os
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup

################################################################
########################### API HELPERS ########################
################################################################

BASE_URL = "https://datasearch.fdz.dezim-institut.de"
PASSWORD = os.environ.get('COLECTICA_PASSWORD')
if PASSWORD is None:
    raise ValueError('The environment variable COLECTICA_PASSWORD is not set.')

# AUTH TOKEN VIA LOCAL JWT PROVIDER
payload = { "username": "fdz@dezim-institut.de", "password": PASSWORD }
response = requests.post(BASE_URL + "/token/CreateToken", json=payload)
token = response.json()

# BASE API REQUEST FUNCTION
def request(path, method, data=None):
    headers = { "Authorization": "Bearer " + token['access_token'] }
    if method == "GET":
        response = requests.get(BASE_URL + "/api/v1/" + path, headers=headers)
    elif method == "POST":
        response = requests.post(BASE_URL  + "/api/v1/" + path, json=data, headers=headers)
    return response.json()


# PRETTY PRINT
def pretty_print(res):
    print(json.dumps(res, indent=2))
    print(" ------------------------------------------------------- ")

################################################################
########################### XML CREATION ########################
################################################################

def attr_from_lang(attribute, lang):
    if isinstance(attribute, dict):
        attribute_array = [attribute]
    if isinstance(attribute, list):
        attribute_array = attribute

    return next((item['#text'] for item in attribute_array if item['@xml:lang'] == lang), None)

def valueFromCustomField(customAttributes, title, fallbackValue):
    for item in customAttributes:
        if item["r:AttributeKey"] == "extension:CustomField":
            value = json.loads(item["r:AttributeValue"])
            if "en-GB" not in value["Title"]:
                return fallbackValue
            if value["Title"]["en-GB"] == title:
                return value["StringValue"]
    return None

def clean_html(html_string):
    soup = BeautifulSoup(html_string, 'html.parser')
    cleaned = soup.get_text()
    cleaned = " ".join(cleaned.split())
    return cleaned

def doiFrom(string):
    return string.split("https://doi.org/")[1].split("</a>")[0].strip()

def create_xml(attributes):
    ##### Root element and header elements ####
    root = ET.Element("resource", {
        "xmlns": "http://da-ra.de/schema/kernel-4",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:noNamespaceSchemaLocation": "http://www.da-ra.de/dara/schemadefinitions/dara.xsd"
    })
    resource_type = ET.SubElement(root, "resourceType")
    resource_type.text = "Dataset"
    resource_identifier = ET.SubElement(root, "resourceIdentifier")
    identifier = ET.SubElement(resource_identifier, "identifier")
    identifier.text = doiFrom(attributes["r:UserID"]["#text"]).split("/")[1]
    current_version = ET.SubElement(resource_identifier, "currentVersion")
    current_version.text = doiFrom(attributes["r:UserID"]["#text"])[-5:]

    #### Title ####
    titles = ET.SubElement(root, "titles")
    for lang in ["de", "en-GB"]:
        value = attr_from_lang(attributes["r:Citation"]["r:Title"]["r:String"], lang)
        if value:
            title = ET.SubElement(titles, "title")
            language = ET.SubElement(title, "language")
            language.text = lang
            title_name = ET.SubElement(title, "titleName")
            title_name.text = value

    #### Creators ####
    creators = ET.SubElement(root, "creators")
    for item in attributes["r:Citation"]["r:Creator"]:
        item = item["r:CreatorName"]
        person = ET.SubElement(creators, "person")
        first_name = ET.SubElement(person, "firstName")
        first_name.text = item["r:String"]["#text"].split(" ")[0] # first word
        middle_name = ET.SubElement(person, "middleName")
        last_name = ET.SubElement(person, "lastName")
        last_name.text = " ".join(item["r:String"]["#text"].split(" ")[1:]) # all words after the first
        affiliation = ET.SubElement(person, "affiliation")
        affiliation_name = ET.SubElement(affiliation, "affiliationName")
        affiliation_name.text = clean_html(item["@affiliation"])

    #### Abstract ####
    descriptions = ET.SubElement(root, "descriptions")
    for lang in ["de", "en-GB"]:
        value = attr_from_lang(attributes["r:Abstract"]["r:Content"], lang)
        if value:
            description = ET.SubElement(descriptions, "description")
            language = ET.SubElement(description, "language")
            language.text = lang
            freextext = ET.SubElement(description, "freetext")
            freextext.text = clean_html(value)
            language = ET.SubElement(description, "descriptionType")
            language.text = "Abstract"

    #### Publisher ####
    publisher = ET.SubElement(root, "publisher")
    institution = ET.SubElement(publisher, "institution")
    institutionName = ET.SubElement(institution, "institutionName")
    institutionName.text = valueFromCustomField(attributes["r:UserAttributePair"], "Responsible Institution", "DeZIM-Institute")

    #### DOI ####
    # this returns a link like <a href=\"https://datasearch.fdz.dezim-institut.de/item/de.dezim/495353c9-1672-477f-aa18-d1d464fcaa12\">https://doi.org/10.34882/dezim.dpd-fac.f.1.2.0</a>
    # Extract the DOI from this string
    doi = doiFrom(attributes["r:UserID"]["#text"])
    keyDoi = ET.SubElement(root, "doiProposal")
    keyDoi.text = doi

    #### dataURL ####
    dataURLs = ET.SubElement(root, "dataURLs")
    dataURL = ET.SubElement(dataURLs, "dataURL")
    dataURL.text = clean_html(attributes["r:UserID"]["#text"])

    #### Export XML to file ####
    filename = doiFrom(attributes["r:UserID"]["#text"]).split("/")[1] + ".xml"
    print("save to file", filename)
    tree = ET.ElementTree(root)
    ET.indent(tree, space="\t", level=0)
    tree.write("data/" + filename, encoding="utf-8")

################################################################
########################### REQUESTS ############################
################################################################

# STATISTICS
statistics = request("repository/statistics", "GET")
# pretty_print(statistics)

# GET ALL STUDIES
payload = {
    "ItemTypes": ["30ea0200-7121-4f01-8d21-a931a182b86d"], # Study ifentifier, see: https://docs.colectica.com/repository/technical/item-type-identifiers/
    "SearchLatestVersion": True
}
res = request("_query", "POST", payload)

# GET METADATA FOR EACH STUDY
for item in res["Results"]:
    res = request("item/de.dezim/" + item["Identifier"], "GET")

    jsonified = xmltodict.parse(res["Item"])
    study_attributes = jsonified["Fragment"]["StudyUnit"]

    create_xml(study_attributes)
